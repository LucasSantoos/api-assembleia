-- associado definition

CREATE TABLE associado (
id varchar(255),
nome varchar(255) not null,
cpf varchar(255) not null,
active boolean not null,
created_at DATETIME(6) not null,
deleted_at DATETIME(6),
updated_at DATETIME(6) not null,
primary key (id));

-- pauta definition

CREATE TABLE pauta (
id varchar(255) not null,
descricao varchar(255) not null,
observacao varchar(255),
data_criacao timestamp,
data_fim_votacao timestamp,
data_inicio_votacao timestamp,
primary key (id));

-- voto definition

CREATE TABLE voto (
id varchar(255) not null,
associado_id varchar(255) not null,
pauta_id varchar(255) not null,
data_criacao timestamp,
opcao boolean not null,
primary key (id),
CONSTRAINT voto_ref_pauta_fk FOREIGN KEY (pauta_id) REFERENCES pauta(id),
CONSTRAINT voto_ref_associado_fk FOREIGN KEY (associado_id) REFERENCES associado(id)
);


