package br.com.cwi.assembleia.domain.voto;

import br.com.cwi.assembleia.domain.validation.ValidationHandler;
import br.com.cwi.assembleia.domain.validation.Validator;

public class VotoValidator extends Validator {

    private final Voto voto;

    protected VotoValidator(final Voto voto, final ValidationHandler handler) {
        super(handler);
        this.voto = voto;
    }

    @Override
    public void validate() {
        checkNameConstraints();
    }

    private void checkNameConstraints() {
    }
}
