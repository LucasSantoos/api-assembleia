package br.com.cwi.assembleia.domain.pauta;

import br.com.cwi.assembleia.domain.validation.Error;
import br.com.cwi.assembleia.domain.validation.ValidationHandler;
import br.com.cwi.assembleia.domain.validation.Validator;

public class PautaValidator extends Validator {

    public static final int NAME_MAX_LENGTH = 255;
    public static final int NAME_MIN_LENGTH = 1;

    private final Pauta pauta;

    protected PautaValidator(final Pauta pauta, final ValidationHandler handler) {
        super(handler);
        this.pauta = pauta;
    }

    @Override
    public void validate() {
        checkNameConstraints();
    }

    private void checkNameConstraints() {
        final var descricao = this.pauta.getDescricao();
        if (descricao == null) {
            this.validationHandler().append(new Error("'descricao' should not be null"));
            return;
        }

        if (descricao.isBlank()) {
            this.validationHandler().append(new Error("'descricao' should not be empty"));
            return;
        }

        final int length = descricao.trim().length();
        if (length > NAME_MAX_LENGTH || length < NAME_MIN_LENGTH) {
            this.validationHandler().append(new Error("'descricao' must be between 1 and 255 characters"));
        }
    }
}
