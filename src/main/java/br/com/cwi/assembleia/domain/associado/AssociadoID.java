package br.com.cwi.assembleia.domain.associado;

import br.com.cwi.assembleia.domain.Identifier;
import br.com.cwi.assembleia.domain.utils.IdUtils;

import java.util.Objects;

public class AssociadoID extends Identifier {

    private final String value;

    private AssociadoID(final String value) {
        Objects.requireNonNull(value);
        this.value = value;
    }

    public static AssociadoID unique() {
        return AssociadoID.from(IdUtils.uuid());
    }

    public static AssociadoID from(final String anId) {
        return new AssociadoID(anId);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final AssociadoID that = (AssociadoID) o;
        return getValue().equals(that.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
}
