package br.com.cwi.assembleia.domain.associado;

import br.com.cwi.assembleia.domain.AggregateRoot;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.utils.InstantUtils;
import br.com.cwi.assembleia.domain.validation.ValidationHandler;
import br.com.cwi.assembleia.domain.validation.handler.Notification;

import java.time.Instant;

public class Associado extends AggregateRoot<AssociadoID> {

    private String nome;
    private String cpf;
    private boolean active;
    private final Instant createdAt;
    private Instant updatedAt;
    private Instant deletedAt;

    protected Associado(
            final AssociadoID id,
            final String nome,
            final String cpf,
            final boolean active,
            final Instant createdAt,
            final Instant updatedAt,
            final Instant deletedAt
    ) {
        super(id);
        this.nome = nome;
        this.cpf = cpf;
        this.active = active;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        selfValidate();
    }

    public static Associado newAssociado(
            final String nome,
            final String cpf,
            final boolean isActive
    ) {
        final var id = AssociadoID.unique();
        final var now = InstantUtils.now();
        final var deletedAt = isActive ? null : now;
        return new Associado(id, nome, cpf, isActive, now, now, deletedAt);
    }

    public static Associado with(final AssociadoID id,
                                 final String nome,
                                 final String cpf,
                                 final boolean active,
                                 final Instant createdAt,
                                 final Instant updatedAt,
                                 final Instant deletedAt) {
        return new Associado(id, nome, cpf, active, createdAt, updatedAt, deletedAt);
    }

    @Override
    public void validate(final ValidationHandler handler) {
        new AssociadoValidator(this, handler).validate();
    }

    public Associado update(final String nome, final String cpf, final boolean isActive) {
        if (isActive) {
            activate();
        } else {
            deactivate();
        }
        this.nome = nome;
        this.cpf = cpf;
        this.updatedAt = InstantUtils.now();
        selfValidate();
        return this;
    }

    public Associado deactivate() {
        if (getDeletedAt() == null) {
            this.deletedAt = InstantUtils.now();
        }
        this.active = false;
        this.updatedAt = InstantUtils.now();
        return this;
    }

    public Associado activate() {
        this.deletedAt = null;
        this.active = true;
        this.updatedAt = InstantUtils.now();
        return this;
    }

    private void selfValidate() {
        final var notification = Notification.create();
        validate(notification);

        if (notification.hasError()) {
            throw new NotificationException("Falha ao criar o agregado Associado", notification);
        }
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public boolean isActive() {
        return active;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public Instant getDeletedAt() {
        return deletedAt;
    }
}
