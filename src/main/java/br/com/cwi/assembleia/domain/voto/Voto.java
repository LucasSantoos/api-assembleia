package br.com.cwi.assembleia.domain.voto;

import br.com.cwi.assembleia.domain.AggregateRoot;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.validation.ValidationHandler;
import br.com.cwi.assembleia.domain.validation.handler.Notification;

import java.time.LocalDateTime;

public class Voto extends AggregateRoot<VotoID> {

    private final AssociadoID associadoID;
    private final PautaID pautaID;
    private Boolean opcao;
    private final LocalDateTime dataCriacao;

    protected Voto(
            final VotoID id,
            final AssociadoID associadoID,
            final PautaID pautaID,
            final Boolean opcao,
            final LocalDateTime dataCriacao
    ) {
        super(id);
        this.associadoID = associadoID;
        this.pautaID = pautaID;
        this.opcao = opcao;
        this.dataCriacao = dataCriacao;
        selfValidate();
    }

    public static Voto newVoto(
            final AssociadoID associadoID,
            final PautaID pautaID,
            final Boolean opcao
    ) {
        final var id = VotoID.unique();
        final var now = LocalDateTime.now();
        return new Voto(id, associadoID, pautaID, opcao, now);
    }

    public static Voto with(final VotoID id,
                            final AssociadoID associadoID,
                            final PautaID pautaID,
                            final Boolean opcao,
                            final LocalDateTime dataCriacao) {
        return new Voto(id, associadoID, pautaID, opcao, dataCriacao);
    }

    @Override
    public void validate(final ValidationHandler handler) {
        new VotoValidator(this, handler).validate();
    }

    public Voto update(final Boolean opcao) {
        this.opcao = opcao;
        selfValidate();
        return this;
    }

    private void selfValidate() {
        final var notification = Notification.create();
        validate(notification);

        if (notification.hasError()) {
            throw new NotificationException("Falha ao criar o agregado Voto", notification);
        }
    }

    public AssociadoID getAssociadoID() {
        return associadoID;
    }

    public PautaID getPautaID() {
        return pautaID;
    }

    public Boolean getOpcao() {
        return opcao;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }
}
