package br.com.cwi.assembleia.domain.voto;

import br.com.cwi.assembleia.domain.Identifier;
import br.com.cwi.assembleia.domain.utils.IdUtils;

import java.util.Objects;

public class VotoID extends Identifier {

    private final String value;

    private VotoID(final String value) {
        Objects.requireNonNull(value);
        this.value = value;
    }

    public static VotoID unique() {
        return VotoID.from(IdUtils.uuid());
    }

    public static VotoID from(final String anId) {
        return new VotoID(anId);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final VotoID that = (VotoID) o;
        return getValue().equals(that.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
}
