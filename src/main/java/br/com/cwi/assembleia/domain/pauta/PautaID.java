package br.com.cwi.assembleia.domain.pauta;

import br.com.cwi.assembleia.domain.Identifier;
import br.com.cwi.assembleia.domain.utils.IdUtils;

import java.util.Objects;

public class PautaID extends Identifier {

    private final String value;

    private PautaID(final String value) {
        Objects.requireNonNull(value);
        this.value = value;
    }

    public static PautaID unique() {
        return PautaID.from(IdUtils.uuid());
    }

    public static PautaID from(final String anId) {
        return new PautaID(anId);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final PautaID that = (PautaID) o;
        return getValue().equals(that.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
}
