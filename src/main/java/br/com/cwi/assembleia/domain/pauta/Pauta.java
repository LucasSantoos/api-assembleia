package br.com.cwi.assembleia.domain.pauta;

import br.com.cwi.assembleia.domain.AggregateRoot;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.validation.ValidationHandler;
import br.com.cwi.assembleia.domain.validation.handler.Notification;
import br.com.cwi.assembleia.domain.voto.Voto;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class Pauta extends AggregateRoot<PautaID> {

    private String descricao;
    private String observacao;
    private final LocalDateTime dataCriacao;
    private LocalDateTime dataInicioVotacao;
    private LocalDateTime dataFimVotacao;
    private final List<Voto> votos;

    protected Pauta(
            final PautaID id,
            final String descricao,
            final String observacao,
            final LocalDateTime dataCriacao,
            final LocalDateTime dataInicioVotacao,
            final LocalDateTime dataFimVotacao,
            final List<Voto> votos
    ) {
        super(id);
        this.descricao = descricao;
        this.observacao = observacao;
        this.dataCriacao = dataCriacao;
        this.dataInicioVotacao = dataInicioVotacao;
        this.dataFimVotacao = dataFimVotacao;
        this.votos = votos;
        selfValidate();
    }

    public static Pauta newPauta(
            final String descricao,
            final String observacao
    ) {
        final var id = PautaID.unique();
        final var now = LocalDateTime.now();
        return new Pauta(id, descricao, observacao, now, null, null, new ArrayList<>());
    }

    public static Pauta with(final PautaID id,
                             final String descricao,
                             final String observacao,
                             final LocalDateTime dataCriacao,
                             final LocalDateTime dataInicioVotacao,
                             final LocalDateTime dataFimVotacao,
                             final List<Voto> votos) {
        return new Pauta(id, descricao, observacao, dataCriacao, dataInicioVotacao, dataFimVotacao, votos);
    }

    public static Pauta with(final Pauta pauta) {
        return new Pauta(
                pauta.id,
                pauta.descricao,
                pauta.observacao,
                pauta.dataCriacao,
                pauta.dataInicioVotacao,
                pauta.dataFimVotacao,
                new ArrayList<>(pauta.votos)
        );
    }

    @Override
    public void validate(final ValidationHandler handler) {
        new PautaValidator(this, handler).validate();
    }

    public Pauta update(final String descricao,
                        final String observacao) {
        this.descricao = descricao;
        this.observacao = observacao;
        selfValidate();
        return this;
    }

    private void selfValidate() {
        final var notification = Notification.create();
        validate(notification);

        if (notification.hasError()) {
            throw new NotificationException("Falha ao criar o agregado Pauta", notification);
        }
    }

    public List<Voto> getVotos() {
        return Collections.unmodifiableList(votos);
    }

    public Pauta addVoto(final Voto aVoto) {
        if (aVoto == null) {
            return this;
        }
        this.votos.add(aVoto);
        return this;
    }

    public Pauta addVotos(final List<Voto> votos) {
        if (votos == null || votos.isEmpty()) {
            return this;
        }
        this.votos.addAll(votos);
        return this;
    }

    public Pauta removeVoto(final Voto aCategoryID) {
        if (aCategoryID == null) {
            return this;
        }
        this.votos.remove(aCategoryID);
        return this;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getObservacao() {
        return observacao;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public LocalDateTime getDataInicioVotacao() {
        return dataInicioVotacao;
    }

    public LocalDateTime getDataFimVotacao() {
        return dataFimVotacao;
    }

    public Map<Boolean, Long> getResultado() {
        return this.votos.stream().collect(Collectors.groupingBy(Voto::getOpcao, Collectors.counting()));
    }

    public Long getTotalVotos() {
        return this.votos.stream().map(Voto::getOpcao).count();
    }

    public Map<Boolean, Long> calcularPorcentagem(Map<Boolean, Long> result, Long totalVotos) {
        Map<Boolean, Long> percentage = new HashMap<>();
        for (Map.Entry<Boolean, Long> entry : result.entrySet()) {
            percentage.put(entry.getKey(), (entry.getValue() * 100) / totalVotos);
        }
        return percentage;
    }

    public Pauta setTempoVotacao(Long tempoSessao) {
        this.dataInicioVotacao = LocalDateTime.now();
        this.dataFimVotacao = LocalDateTime.now().plusMinutes(tempoSessao);
        return this;
    }
}
