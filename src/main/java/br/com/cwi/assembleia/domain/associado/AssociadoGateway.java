package br.com.cwi.assembleia.domain.associado;

import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;

import java.util.List;
import java.util.Optional;

public interface AssociadoGateway {

    Associado create(Associado associado);

    void deleteById(AssociadoID id);

    Optional<Associado> findById(AssociadoID id);

    Associado update(Associado associado);

    Pagination<Associado> findAll(SearchQuery aQuery);

    List<AssociadoID> existsByIds(Iterable<AssociadoID> ids);

}
