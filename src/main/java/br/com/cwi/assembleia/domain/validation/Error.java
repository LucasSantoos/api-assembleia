package br.com.cwi.assembleia.domain.validation;

public record Error(String message) {
}
