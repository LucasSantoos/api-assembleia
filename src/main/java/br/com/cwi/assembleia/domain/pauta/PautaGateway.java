package br.com.cwi.assembleia.domain.pauta;

import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;
import br.com.cwi.assembleia.infrastructure.pauta.persistence.PautaEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PautaGateway {

    Pauta create(Pauta pauta);

    void deleteById(PautaID id);

    Optional<Pauta> findById(PautaID id);

    Pauta update(Pauta pauta);

    Pagination<Pauta> findAll(SearchQuery aQuery);

    List<PautaID> existsByIds(Iterable<PautaID> ids);

    Optional<PautaEntity> getByPautaCodigoAndBetweenSessao(String id, LocalDateTime data);

}
