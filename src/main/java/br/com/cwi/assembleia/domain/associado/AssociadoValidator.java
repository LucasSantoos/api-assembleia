package br.com.cwi.assembleia.domain.associado;

import br.com.cwi.assembleia.domain.validation.Error;
import br.com.cwi.assembleia.domain.validation.ValidationHandler;
import br.com.cwi.assembleia.domain.validation.Validator;

public class AssociadoValidator extends Validator {

    public static final int NAME_MAX_LENGTH = 255;
    public static final int NAME_MIN_LENGTH = 1;

    private final Associado associado;

    protected AssociadoValidator(final Associado associado, final ValidationHandler handler) {
        super(handler);
        this.associado = associado;
    }

    @Override
    public void validate() {
        checkNameConstraints();
    }

    private void checkNameConstraints() {
        final var nome = this.associado.getNome();
        if (nome == null) {
            this.validationHandler().append(new Error("'nome' should not be null"));
            return;
        }

        if (nome.isBlank()) {
            this.validationHandler().append(new Error("'nome' should not be empty"));
            return;
        }

        final int length = nome.trim().length();
        if (length > NAME_MAX_LENGTH || length < NAME_MIN_LENGTH) {
            this.validationHandler().append(new Error("'nome' must be between 1 and 255 characters"));
        }
    }
}
