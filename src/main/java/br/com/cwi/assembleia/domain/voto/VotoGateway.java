package br.com.cwi.assembleia.domain.voto;

import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;

import java.util.List;
import java.util.Optional;

public interface VotoGateway {

    Voto create(Voto voto);

    void deleteById(VotoID id);

    Optional<Voto> findById(VotoID id);

    Voto update(Voto voto);

    Pagination<Voto> findAll(SearchQuery aQuery);

    List<VotoID> existsByIds(Iterable<VotoID> ids);

    List<String> existsByAssociadoAndPautaId(String associadoID, String pautaID);

}
