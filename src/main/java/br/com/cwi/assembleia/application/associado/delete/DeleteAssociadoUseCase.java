package br.com.cwi.assembleia.application.associado.delete;

import br.com.cwi.assembleia.application.UnitUseCase;
import br.com.cwi.assembleia.domain.associado.AssociadoGateway;
import br.com.cwi.assembleia.domain.associado.AssociadoID;

import java.util.Objects;

public class DeleteAssociadoUseCase extends UnitUseCase<String> {

    private final AssociadoGateway associadoGateway;

    public DeleteAssociadoUseCase(
            final AssociadoGateway associadoGateway
    ) {
        this.associadoGateway = Objects.requireNonNull(associadoGateway);
    }

    @Override
    public void execute(String in) {
        this.associadoGateway.deleteById(AssociadoID.from(in));
    }
}
