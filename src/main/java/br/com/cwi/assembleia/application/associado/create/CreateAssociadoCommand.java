package br.com.cwi.assembleia.application.associado.create;

public record CreateAssociadoCommand(
        String nome,
        String cpf,
        boolean isActive
) {

    public static CreateAssociadoCommand with(
            final String nome,
            final String cpf,
            final Boolean isActive
    ) {
        return new CreateAssociadoCommand(nome, cpf, isActive != null ? isActive : true);
    }
}
