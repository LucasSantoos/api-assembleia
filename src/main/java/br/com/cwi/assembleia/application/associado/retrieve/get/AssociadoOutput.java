package br.com.cwi.assembleia.application.associado.retrieve.get;


import br.com.cwi.assembleia.domain.associado.Associado;

import java.time.Instant;

public record AssociadoOutput(
        String id,
        String nome,
        String cpf,
        boolean isActive,
        Instant createdAt,
        Instant updatedAt,
        Instant deletedAt
) {

    public static AssociadoOutput from(final Associado associado) {
        return new AssociadoOutput(
                associado.getId().getValue(),
                associado.getNome(),
                associado.getCpf(),
                associado.isActive(),
                associado.getCreatedAt(),
                associado.getUpdatedAt(),
                associado.getDeletedAt()
        );
    }
}
