package br.com.cwi.assembleia.application.pauta.start;

import br.com.cwi.assembleia.domain.pauta.Pauta;

import java.time.LocalDateTime;

public record StartPautaOutput(
        String id,
        String descricao,
        String observacao,
        LocalDateTime dataInicioVotacao,
        LocalDateTime dataFimVotacao
) {
    public static StartPautaOutput from(final Pauta pauta) {
        return new StartPautaOutput(
                pauta.getId().getValue(),
                pauta.getDescricao(),
                pauta.getObservacao(),
                pauta.getDataInicioVotacao(),
                pauta.getDataFimVotacao()
        );
    }

}
