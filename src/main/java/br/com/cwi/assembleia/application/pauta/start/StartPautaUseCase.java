package br.com.cwi.assembleia.application.pauta.start;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.Identifier;
import br.com.cwi.assembleia.domain.exceptions.DomainException;
import br.com.cwi.assembleia.domain.exceptions.NotFoundException;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.pauta.Pauta;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.validation.handler.Notification;

import java.util.Objects;
import java.util.function.Supplier;

public class StartPautaUseCase extends UseCase<StartPautaCommand, StartPautaOutput> {

    private final PautaGateway pautaGateway;

    public StartPautaUseCase(
            final PautaGateway pautaGateway
    ) {
        this.pautaGateway = Objects.requireNonNull(pautaGateway);
    }

    @Override
    public StartPautaOutput execute(final StartPautaCommand command) {
        final var anId = PautaID.from(command.id());
        final var minutes = command.minutes();

        final var pauta = this.pautaGateway.findById(anId)
                .orElseThrow(notFound(anId));

        final var notification = Notification.create();
        notification.validate(() -> pauta.setTempoVotacao(minutes));

        if (notification.hasError()) {
            throw new NotificationException(
                    "Could not start Pauta %s".formatted(command.id()), notification
            );
        }

        return StartPautaOutput.from(this.pautaGateway.update(pauta));
    }

    private Supplier<DomainException> notFound(final Identifier anId) {
        return () -> NotFoundException.with(Pauta.class, anId);
    }
}
