package br.com.cwi.assembleia.application.pauta.retrieve.result;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.pauta.Pauta;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.validation.handler.Notification;

import java.util.Objects;

public class ResultPautaUseCase extends UseCase<String, PautaResultOutput> {

    private final PautaGateway pautaGateway;

    public ResultPautaUseCase(
            final PautaGateway pautaGateway
    ) {
        this.pautaGateway = Objects.requireNonNull(pautaGateway);
    }

    @Override
    public PautaResultOutput execute(String in) {
        final var notification = Notification.create();
        final var optPauta = pautaGateway.findById(PautaID.from(in))
                .orElseThrow(() -> new NotificationException("Unable to fetch poll results", notification));

        Pauta pauta = Pauta.with(optPauta.getId(),
                optPauta.getDescricao(),
                optPauta.getObservacao(),
                optPauta.getDataCriacao(),
                optPauta.getDataInicioVotacao(),
                optPauta.getDataFimVotacao(),
                optPauta.getVotos());

        final var result = pauta.getResultado();
        final var totalVotos = pauta.getTotalVotos();
        final var porcentagem = pauta.calcularPorcentagem(result, totalVotos);

        return new PautaResultOutput(pauta.getId().getValue(),
                pauta.getDescricao(),
                pauta.getObservacao(),
                result,
                porcentagem,
                totalVotos
        );
    }
}
