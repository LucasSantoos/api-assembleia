package br.com.cwi.assembleia.application.associado.create;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.associado.Associado;
import br.com.cwi.assembleia.domain.associado.AssociadoGateway;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.validation.handler.Notification;

import java.util.Objects;

public class CreateAssociadoUseCase extends UseCase<CreateAssociadoCommand, CreateAssociadoOutput> {

    private final AssociadoGateway associadoGateway;

    public CreateAssociadoUseCase(
            final AssociadoGateway associadoGateway
    ) {
        this.associadoGateway = Objects.requireNonNull(associadoGateway);
    }

    @Override
    public CreateAssociadoOutput execute(final CreateAssociadoCommand command) {
        final var nome = command.nome();
        final var cpf = command.cpf();
        final var isActive = command.isActive();

        final var notification = Notification.create();
        final var associado = notification.validate(() -> Associado.newAssociado(nome, cpf, isActive));

        if (notification.hasError()) {
            throw new NotificationException("Could not create Aggregate Associado", notification);
        }

        return CreateAssociadoOutput.from(this.associadoGateway.create(associado));
    }
}
