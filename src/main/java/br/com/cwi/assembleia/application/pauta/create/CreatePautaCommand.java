package br.com.cwi.assembleia.application.pauta.create;

import java.time.LocalDateTime;

public record CreatePautaCommand(
        String descricao,
        String observacao,
        LocalDateTime dataInicioVotacao,
        LocalDateTime dataFimVotacao
) {

    public static CreatePautaCommand with(
            final String descricao,
            final String observacao,
            final LocalDateTime dataInicioVotacao,
            final LocalDateTime dataFimVotacao
    ) {
        return new CreatePautaCommand(descricao, observacao, dataInicioVotacao, dataFimVotacao);
    }
}
