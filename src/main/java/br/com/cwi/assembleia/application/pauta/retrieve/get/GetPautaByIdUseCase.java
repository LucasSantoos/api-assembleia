package br.com.cwi.assembleia.application.pauta.retrieve.get;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.exceptions.NotFoundException;
import br.com.cwi.assembleia.domain.pauta.Pauta;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;

import java.util.Objects;

public class GetPautaByIdUseCase extends UseCase<String, PautaOutput> {

    private final PautaGateway pautaGateway;

    public GetPautaByIdUseCase(
            final PautaGateway pautaGateway
    ) {
        this.pautaGateway = Objects.requireNonNull(pautaGateway);
    }

    @Override
    public PautaOutput execute(String in) {
        final var pautaID = PautaID.from(in);
        return this.pautaGateway.findById(pautaID)
                .map(PautaOutput::from)
                .orElseThrow(() -> NotFoundException.with(Pauta.class, pautaID));
    }
}
