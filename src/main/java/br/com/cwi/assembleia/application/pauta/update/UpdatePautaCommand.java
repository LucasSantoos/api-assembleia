package br.com.cwi.assembleia.application.pauta.update;

public record UpdatePautaCommand(
        String id,
        String descricao,
        String observacao
) {

    public static UpdatePautaCommand with(
            final String id,
            final String descricao,
            final String observacao
    ) {
        return new UpdatePautaCommand(id, descricao, observacao);
    }
}
