package br.com.cwi.assembleia.application;

public abstract class UnitUseCase<IN> {

    public abstract void execute(IN in);
}
