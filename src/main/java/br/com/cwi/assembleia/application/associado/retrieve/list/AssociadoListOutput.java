package br.com.cwi.assembleia.application.associado.retrieve.list;

import br.com.cwi.assembleia.domain.associado.Associado;

import java.time.Instant;

public record AssociadoListOutput(
        String id,
        String nome,
        String cpf,
        boolean isActive,
        Instant createdAt,
        Instant deletedAt
) {

    public static AssociadoListOutput from(final Associado associado) {
        return new AssociadoListOutput(
                associado.getId().getValue(),
                associado.getNome(),
                associado.getCpf(),
                associado.isActive(),
                associado.getCreatedAt(),
                associado.getDeletedAt()
        );
    }
}
