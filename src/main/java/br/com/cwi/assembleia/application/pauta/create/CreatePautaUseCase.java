package br.com.cwi.assembleia.application.pauta.create;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.pauta.Pauta;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.validation.handler.Notification;

import java.util.Objects;

public class CreatePautaUseCase extends UseCase<CreatePautaCommand, CreatePautaOutput> {

    private final PautaGateway pautaGateway;

    public CreatePautaUseCase(
            final PautaGateway pautaGateway
    ) {
        this.pautaGateway = Objects.requireNonNull(pautaGateway);
    }

    @Override
    public CreatePautaOutput execute(final CreatePautaCommand command) {
        final var descricao = command.descricao();
        final var observacao = command.observacao();

        final var notification = Notification.create();
        final var pauta = notification.validate(() -> Pauta.newPauta(descricao, observacao));

        if (notification.hasError()) {
            throw new NotificationException("Could not create Aggregate Pauta", notification);
        }

        return CreatePautaOutput.from(this.pautaGateway.create(pauta));
    }
}
