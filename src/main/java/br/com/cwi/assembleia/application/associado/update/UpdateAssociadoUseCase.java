package br.com.cwi.assembleia.application.associado.update;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.Identifier;
import br.com.cwi.assembleia.domain.associado.Associado;
import br.com.cwi.assembleia.domain.associado.AssociadoGateway;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.exceptions.DomainException;
import br.com.cwi.assembleia.domain.exceptions.NotFoundException;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.validation.handler.Notification;

import java.util.Objects;
import java.util.function.Supplier;

public class UpdateAssociadoUseCase extends UseCase<UpdateAssociadoCommand, UpdateAssociadoOutput> {

    private final AssociadoGateway associadoGateway;

    public UpdateAssociadoUseCase(
            final AssociadoGateway associadoGateway
    ) {
        this.associadoGateway = Objects.requireNonNull(associadoGateway);
    }

    @Override
    public UpdateAssociadoOutput execute(final UpdateAssociadoCommand command) {
        final var anId = AssociadoID.from(command.id());
        final var nome = command.nome();
        final var cpf = command.cpf();
        final var isActive = command.isActive();

        final var associado = this.associadoGateway.findById(anId)
                .orElseThrow(notFound(anId));

        final var notification = Notification.create();
        notification.validate(() -> associado.update(nome, cpf, isActive));

        if (notification.hasError()) {
            throw new NotificationException(
                    "Could not update Aggregate Associado %s".formatted(command.id()), notification
            );
        }

        return UpdateAssociadoOutput.from(this.associadoGateway.update(associado));
    }

    private Supplier<DomainException> notFound(final Identifier anId) {
        return () -> NotFoundException.with(Associado.class, anId);
    }
}
