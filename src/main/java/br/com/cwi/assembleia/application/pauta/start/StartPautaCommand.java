package br.com.cwi.assembleia.application.pauta.start;

public record StartPautaCommand(
        String id,
        Long minutes
) {

    public static StartPautaCommand with(
            final String id,
            final Long minutes
    ) {
        return new StartPautaCommand(id, minutes);
    }
}
