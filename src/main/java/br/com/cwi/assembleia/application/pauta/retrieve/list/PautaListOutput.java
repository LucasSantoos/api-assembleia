package br.com.cwi.assembleia.application.pauta.retrieve.list;

import br.com.cwi.assembleia.domain.pauta.Pauta;

import java.time.LocalDateTime;

public record PautaListOutput(
        String id,
        String descricao,
        String observacao,
        LocalDateTime dataCriacao,
        LocalDateTime dataInicioVotacao,
        LocalDateTime dataFimVotacao
) {

    public static PautaListOutput from(final Pauta pauta) {
        return new PautaListOutput(
                pauta.getId().getValue(),
                pauta.getDescricao(),
                pauta.getObservacao(),
                pauta.getDataCriacao(),
                pauta.getDataInicioVotacao(),
                pauta.getDataFimVotacao()
        );
    }
}
