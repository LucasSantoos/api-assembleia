package br.com.cwi.assembleia.application.associado.update;

import br.com.cwi.assembleia.domain.associado.Associado;

public record UpdateAssociadoOutput(String id) {

    public static UpdateAssociadoOutput from(final Associado associado) {
        return new UpdateAssociadoOutput(associado.getId().getValue());
    }
}
