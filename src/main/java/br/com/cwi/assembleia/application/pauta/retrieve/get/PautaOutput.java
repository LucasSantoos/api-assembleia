package br.com.cwi.assembleia.application.pauta.retrieve.get;


import br.com.cwi.assembleia.domain.pauta.Pauta;

import java.time.LocalDateTime;
import java.util.List;

public record PautaOutput(
        String id,
        String descricao,
        String observacao,
        LocalDateTime dataCriacao,
        LocalDateTime dataInicioVotacao,
        LocalDateTime dataFimVotacao,
        List<String> votos
) {

    public static PautaOutput from(final Pauta pauta) {
        return new PautaOutput(
                pauta.getId().getValue(),
                pauta.getDescricao(),
                pauta.getObservacao(),
                pauta.getDataCriacao(),
                pauta.getDataInicioVotacao(),
                pauta.getDataFimVotacao(),
                pauta.getVotos().stream()
                        .map(i -> i.getId().getValue())
                        .toList()
        );
    }
}
