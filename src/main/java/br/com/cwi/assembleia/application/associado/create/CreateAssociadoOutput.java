package br.com.cwi.assembleia.application.associado.create;


import br.com.cwi.assembleia.domain.associado.Associado;

public record CreateAssociadoOutput(
        String id
) {

    public static CreateAssociadoOutput from(final String id) {
        return new CreateAssociadoOutput(id);
    }

    public static CreateAssociadoOutput from(final Associado associado) {
        return new CreateAssociadoOutput(associado.getId().getValue());
    }
}
