package br.com.cwi.assembleia.application.pauta.update;

import br.com.cwi.assembleia.domain.pauta.Pauta;

public record UpdatePautaOutput(String id) {

    public static UpdatePautaOutput from(final Pauta pauta) {
        return new UpdatePautaOutput(pauta.getId().getValue());
    }
}
