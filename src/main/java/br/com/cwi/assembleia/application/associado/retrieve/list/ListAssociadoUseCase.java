package br.com.cwi.assembleia.application.associado.retrieve.list;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.associado.AssociadoGateway;
import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;

import java.util.Objects;

public class ListAssociadoUseCase extends UseCase<SearchQuery, Pagination<AssociadoListOutput>> {

    private final AssociadoGateway associadoGateway;

    public ListAssociadoUseCase(
            final AssociadoGateway associadoGateway
    ) {
        this.associadoGateway = Objects.requireNonNull(associadoGateway);
    }

    @Override
    public Pagination<AssociadoListOutput> execute(final SearchQuery aQuery) {
        return this.associadoGateway.findAll(aQuery).map(AssociadoListOutput::from);
    }
}
