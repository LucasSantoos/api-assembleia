package br.com.cwi.assembleia.application.pauta.update;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.Identifier;
import br.com.cwi.assembleia.domain.exceptions.DomainException;
import br.com.cwi.assembleia.domain.exceptions.NotFoundException;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.pauta.Pauta;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.validation.handler.Notification;

import java.util.Objects;
import java.util.function.Supplier;

public class UpdatePautaUseCase extends UseCase<UpdatePautaCommand, UpdatePautaOutput> {

    private final PautaGateway pautaGateway;

    public UpdatePautaUseCase(
            final PautaGateway pautaGateway
    ) {
        this.pautaGateway = Objects.requireNonNull(pautaGateway);
    }

    @Override
    public UpdatePautaOutput execute(final UpdatePautaCommand command) {
        final var anId = PautaID.from(command.id());
        final var descricao = command.descricao();
        final var observacao = command.observacao();

        final var pauta = this.pautaGateway.findById(anId)
                .orElseThrow(notFound(anId));

        final var notification = Notification.create();
        notification.validate(() -> pauta.update(descricao, observacao));

        if (notification.hasError()) {
            throw new NotificationException(
                    "Could not update Aggregate Pauta %s".formatted(command.id()), notification
            );
        }

        return UpdatePautaOutput.from(this.pautaGateway.update(pauta));
    }

    private Supplier<DomainException> notFound(final Identifier anId) {
        return () -> NotFoundException.with(Pauta.class, anId);
    }
}
