package br.com.cwi.assembleia.application.pauta.retrieve.list;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;

import java.util.Objects;

public class ListPautaUseCase extends UseCase<SearchQuery, Pagination<PautaListOutput>> {

    private final PautaGateway pautaGateway;

    public ListPautaUseCase(
            final PautaGateway pautaGateway
    ) {
        this.pautaGateway = Objects.requireNonNull(pautaGateway);
    }

    @Override
    public Pagination<PautaListOutput> execute(final SearchQuery aQuery) {
        return this.pautaGateway.findAll(aQuery).map(PautaListOutput::from);
    }
}
