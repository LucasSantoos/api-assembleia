package br.com.cwi.assembleia.application.pauta.delete;

import br.com.cwi.assembleia.application.UnitUseCase;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;

import java.util.Objects;

public class DeletePautaUseCase extends UnitUseCase<String> {

    private final PautaGateway pautaGateway;

    public DeletePautaUseCase(
            final PautaGateway pautaGateway
    ) {
        this.pautaGateway = Objects.requireNonNull(pautaGateway);
    }

    @Override
    public void execute(String in) {
        this.pautaGateway.deleteById(PautaID.from(in));
    }
}
