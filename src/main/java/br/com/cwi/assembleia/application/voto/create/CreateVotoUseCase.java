package br.com.cwi.assembleia.application.voto.create;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.validation.Error;
import br.com.cwi.assembleia.domain.validation.ValidationHandler;
import br.com.cwi.assembleia.domain.validation.handler.Notification;
import br.com.cwi.assembleia.domain.voto.Voto;
import br.com.cwi.assembleia.domain.voto.VotoGateway;

import java.time.LocalDateTime;
import java.util.Objects;

public class CreateVotoUseCase extends UseCase<CreateVotoCommand, CreateVotoOutput> {

    private final PautaGateway pautaGateway;
    private final VotoGateway votoGateway;

    public CreateVotoUseCase(
            final PautaGateway pautaGateway,
            final VotoGateway votoGateway
    ) {
        this.pautaGateway = Objects.requireNonNull(pautaGateway);
        this.votoGateway = Objects.requireNonNull(votoGateway);
    }

    @Override
    public CreateVotoOutput execute(final CreateVotoCommand command) {
        final var associadoID = AssociadoID.from(command.associadoID());
        final var pautaID = PautaID.from(command.pautaID());
        final var opcao = command.opcao();

        final var notification = Notification.create();
        notification.append(validate(command));
        final var voto = notification.validate(() -> Voto.newVoto(associadoID, pautaID, opcao));

        if (notification.hasError()) {
            throw new NotificationException("Could not create Aggregate Voto", notification);
        }

        return CreateVotoOutput.from(this.votoGateway.create(voto));
    }

    private ValidationHandler validate(final CreateVotoCommand command) {
        final var notification = Notification.create();
        final var associadoID = AssociadoID.from(command.associadoID()).getValue();
        final var pautaID = PautaID.from(command.pautaID()).getValue();

        if (!isPautaAvailable(pautaID)) {
            notification.append(new Error("Pauta it's unavailable"));
        }

        if (existsByAssociadoAndPauta(associadoID, pautaID)) {
            notification.append(new Error("There is already a vote registered for this"));
        }

        return notification;
    }

    public Boolean isPautaAvailable(String pautaID) {
        return this.pautaGateway.getByPautaCodigoAndBetweenSessao(pautaID, LocalDateTime.now())
                .map(obj -> Boolean.TRUE).orElse(Boolean.FALSE);
    }

    public Boolean existsByAssociadoAndPauta(String associadoID, String pautaID) {
        return this.votoGateway.existsByAssociadoAndPautaId(associadoID, pautaID)
                .stream().count() > 0;
    }
}
