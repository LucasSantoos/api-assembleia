package br.com.cwi.assembleia.application.voto.create;

public record CreateVotoCommand(
        String associadoID,
        String pautaID,
        boolean opcao
) {

    public static CreateVotoCommand with(
            final String associadoID,
            final String pautaID,
            final boolean opcao
    ) {
        return new CreateVotoCommand(associadoID, pautaID, opcao);
    }
}
