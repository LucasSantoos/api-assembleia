package br.com.cwi.assembleia.application;

public abstract class UseCase<IN, OUT> {

    public abstract OUT execute(IN in);
}