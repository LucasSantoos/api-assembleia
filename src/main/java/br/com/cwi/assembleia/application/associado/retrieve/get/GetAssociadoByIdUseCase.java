package br.com.cwi.assembleia.application.associado.retrieve.get;

import br.com.cwi.assembleia.application.UseCase;
import br.com.cwi.assembleia.domain.associado.Associado;
import br.com.cwi.assembleia.domain.associado.AssociadoGateway;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.exceptions.NotFoundException;

import java.util.Objects;

public class GetAssociadoByIdUseCase extends UseCase<String, AssociadoOutput> {

    private final AssociadoGateway associadoGateway;

    public GetAssociadoByIdUseCase(
            final AssociadoGateway associadoGateway
    ) {
        this.associadoGateway = Objects.requireNonNull(associadoGateway);
    }

    @Override
    public AssociadoOutput execute(String in) {
        final var associadoID = AssociadoID.from(in);
        return this.associadoGateway.findById(associadoID)
                .map(AssociadoOutput::from)
                .orElseThrow(() -> NotFoundException.with(Associado.class, associadoID));
    }
}
