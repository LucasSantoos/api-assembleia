package br.com.cwi.assembleia.application.associado.update;

public record UpdateAssociadoCommand(
        String id,
        String nome,
        String cpf,
        boolean isActive
) {

    public static UpdateAssociadoCommand with(
            final String id,
            final String nome,
            final String cpf,
            final Boolean isActive
    ) {
        return new UpdateAssociadoCommand(id, nome, cpf, isActive != null ? isActive : true);
    }
}
