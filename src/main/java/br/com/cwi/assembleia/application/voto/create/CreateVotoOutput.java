package br.com.cwi.assembleia.application.voto.create;


import br.com.cwi.assembleia.domain.voto.Voto;

public record CreateVotoOutput(
        String id
) {

    public static CreateVotoOutput from(final String id) {
        return new CreateVotoOutput(id);
    }

    public static CreateVotoOutput from(final Voto voto) {
        return new CreateVotoOutput(voto.getId().getValue());
    }
}
