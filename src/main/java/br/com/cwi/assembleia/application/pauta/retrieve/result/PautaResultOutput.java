package br.com.cwi.assembleia.application.pauta.retrieve.result;

import java.util.Map;

public record PautaResultOutput(
        String id,
        String descricao,
        String observacao,
        Map<Boolean, Long> result,
        Map<Boolean, Long> percentage,
        Long totalVoto
) {
}
