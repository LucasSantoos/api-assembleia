package br.com.cwi.assembleia.application.pauta.create;


import br.com.cwi.assembleia.domain.pauta.Pauta;

public record CreatePautaOutput(
        String id
) {

    public static CreatePautaOutput from(final String id) {
        return new CreatePautaOutput(id);
    }

    public static CreatePautaOutput from(final Pauta pauta) {
        return new CreatePautaOutput(pauta.getId().getValue());
    }
}
