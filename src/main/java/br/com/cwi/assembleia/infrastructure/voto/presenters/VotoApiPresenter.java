package br.com.cwi.assembleia.infrastructure.voto.presenters;

import br.com.cwi.assembleia.application.associado.retrieve.get.AssociadoOutput;
import br.com.cwi.assembleia.application.associado.retrieve.list.AssociadoListOutput;
import br.com.cwi.assembleia.infrastructure.voto.model.VotoListResponse;
import br.com.cwi.assembleia.infrastructure.voto.model.VotoResponse;

public interface VotoApiPresenter {

    static VotoResponse present(final AssociadoOutput output) {
        return new VotoResponse(
                output.id(),
                output.nome(),
                output.cpf(),
                output.isActive(),
                output.createdAt(),
                output.updatedAt(),
                output.deletedAt()
        );
    }

    static VotoListResponse present(final AssociadoListOutput output) {
        return new VotoListResponse(
                output.id(),
                output.nome(),
                output.cpf(),
                output.isActive(),
                output.createdAt(),
                output.deletedAt()
        );
    }
}
