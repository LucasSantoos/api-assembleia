package br.com.cwi.assembleia.infrastructure.voto.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record UpdateVotoRequest(
        @JsonProperty("associado_id") String associadoID,
        @JsonProperty("pauta_id") String pautaID,
        @JsonProperty("voto") Boolean voto,
        @JsonProperty("is_active") Boolean active
) {
    public boolean isActive() {
        return this.active != null ? this.active : true;
    }

    public boolean isVoto() {
        return this.voto != null ? this.voto : true;
    }
}
