package br.com.cwi.assembleia.infrastructure.associado.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public record AssociadoListResponse(
        @JsonProperty("id") String id,
        @JsonProperty("nome") String nome,
        @JsonProperty("cpf") String cpf,
        @JsonProperty("is_active") Boolean active,
        @JsonProperty("created_at") Instant createdAt,
        @JsonProperty("deleted_at") Instant deletedAt
) {
}
