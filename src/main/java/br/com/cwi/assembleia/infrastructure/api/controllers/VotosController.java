package br.com.cwi.assembleia.infrastructure.api.controllers;

import br.com.cwi.assembleia.application.voto.create.CreateVotoCommand;
import br.com.cwi.assembleia.application.voto.create.CreateVotoUseCase;
import br.com.cwi.assembleia.infrastructure.api.VotoAPI;
import br.com.cwi.assembleia.infrastructure.voto.model.CreateVotoRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
public class VotosController implements VotoAPI {

    private final CreateVotoUseCase createVotoUseCase;

    public VotosController(CreateVotoUseCase createVotoUseCase) {
        this.createVotoUseCase = createVotoUseCase;
    }

    @Override
    public ResponseEntity<?> create(final CreateVotoRequest input) {
        final var command = CreateVotoCommand.with(
                input.associadoID(),
                input.pautaID(),
                input.opcao()
        );

        final var output = this.createVotoUseCase.execute(command);

        return ResponseEntity.created(URI.create("/votos/" + output.id())).body(output);
    }
}
