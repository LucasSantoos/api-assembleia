package br.com.cwi.assembleia.infrastructure.pauta.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public record PautaListResponse(
        @JsonProperty("id") String id,
        @JsonProperty("descricao") String descricao,
        @JsonProperty("observacao") String observacao,
        @JsonProperty("data_criacao") LocalDateTime dataCriacao,
        @JsonProperty("data_inicio_votacao") LocalDateTime dataInicioVotacao,
        @JsonProperty("data_fim_votacao") LocalDateTime dataFimVotacao
) {
}
