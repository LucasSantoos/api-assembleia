package br.com.cwi.assembleia.infrastructure.configuration.usecases;

import br.com.cwi.assembleia.application.pauta.create.CreatePautaUseCase;
import br.com.cwi.assembleia.application.pauta.delete.DeletePautaUseCase;
import br.com.cwi.assembleia.application.pauta.retrieve.get.GetPautaByIdUseCase;
import br.com.cwi.assembleia.application.pauta.retrieve.list.ListPautaUseCase;
import br.com.cwi.assembleia.application.pauta.retrieve.result.ResultPautaUseCase;
import br.com.cwi.assembleia.application.pauta.start.StartPautaUseCase;
import br.com.cwi.assembleia.application.pauta.update.UpdatePautaUseCase;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PautaUseCaseConfig {

    private final PautaGateway pautaGateway;

    public PautaUseCaseConfig(final PautaGateway pautaGateway) {
        this.pautaGateway = pautaGateway;
    }

    @Bean
    public CreatePautaUseCase createPautaUseCase() {
        return new CreatePautaUseCase(pautaGateway);
    }

    @Bean
    public DeletePautaUseCase deletePautaUseCase() {
        return new DeletePautaUseCase(pautaGateway);
    }

    @Bean
    public GetPautaByIdUseCase getPautaByIdUseCase() {
        return new GetPautaByIdUseCase(pautaGateway);
    }

    @Bean
    public ListPautaUseCase listPautaUseCase() {
        return new ListPautaUseCase(pautaGateway);
    }

    @Bean
    public UpdatePautaUseCase updatePautaUseCase() {
        return new UpdatePautaUseCase(pautaGateway);
    }

    @Bean
    public ResultPautaUseCase resultPautaUseCase() {
        return new ResultPautaUseCase(pautaGateway);
    }

    @Bean
    public StartPautaUseCase startPautaUseCase() {
        return new StartPautaUseCase(pautaGateway);
    }
}
