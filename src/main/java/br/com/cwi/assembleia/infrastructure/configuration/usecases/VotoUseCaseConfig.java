package br.com.cwi.assembleia.infrastructure.configuration.usecases;

import br.com.cwi.assembleia.application.voto.create.CreateVotoUseCase;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.voto.VotoGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VotoUseCaseConfig {

    private final PautaGateway pautaGateway;
    private final VotoGateway votoGateway;

    public VotoUseCaseConfig(final PautaGateway pautaGateway,
                             final VotoGateway votoGateway) {
        this.pautaGateway = pautaGateway;
        this.votoGateway = votoGateway;
    }

    @Bean
    public CreateVotoUseCase createVotoUseCase() {
        return new CreateVotoUseCase(pautaGateway, votoGateway);
    }
}
