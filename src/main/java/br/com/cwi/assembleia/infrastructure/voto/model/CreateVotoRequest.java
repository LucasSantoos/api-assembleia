package br.com.cwi.assembleia.infrastructure.voto.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CreateVotoRequest(
        @JsonProperty("associado_id") String associadoID,
        @JsonProperty("pauta_id") String pautaID,
        @JsonProperty("opcao") Boolean opcao
) {
    public boolean getOpcao() {
        return this.opcao != null ? this.opcao : true;
    }
}
