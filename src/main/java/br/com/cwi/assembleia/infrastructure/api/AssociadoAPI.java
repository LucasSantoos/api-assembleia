package br.com.cwi.assembleia.infrastructure.api;

import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.infrastructure.associado.model.AssociadoListResponse;
import br.com.cwi.assembleia.infrastructure.associado.model.AssociadoResponse;
import br.com.cwi.assembleia.infrastructure.associado.model.CreateAssociadoRequest;
import br.com.cwi.assembleia.infrastructure.associado.model.UpdateAssociadoRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "associados")
public interface AssociadoAPI {

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Create a new associado")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created successfully"),
            @ApiResponse(responseCode = "422", description = "A validation error was thrown"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    ResponseEntity<?> create(@RequestBody CreateAssociadoRequest input);

    @GetMapping
    @Operation(summary = "List all associados paginated")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Listed successfully"),
            @ApiResponse(responseCode = "422", description = "A invalid parameter was received"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    Pagination<AssociadoListResponse> list(
            @RequestParam(name = "search", required = false, defaultValue = "") final String search,
            @RequestParam(name = "page", required = false, defaultValue = "0") final int page,
            @RequestParam(name = "perPage", required = false, defaultValue = "10") final int perPage,
            @RequestParam(name = "sort", required = false, defaultValue = "nome") final String sort,
            @RequestParam(name = "dir", required = false, defaultValue = "asc") final String direction
    );

    @GetMapping(
            value = "{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Get a associado by it's identifier")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Associado retrieved successfully"),
            @ApiResponse(responseCode = "404", description = "Associado was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    AssociadoResponse getById(@PathVariable(name = "id") String id);

    @PutMapping(
            value = "{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Update a associado by it's identifier")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Associado updated successfully"),
            @ApiResponse(responseCode = "404", description = "Associado was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    ResponseEntity<?> updateById(@PathVariable(name = "id") String id, @RequestBody UpdateAssociadoRequest input);

    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete a associado by it's identifier")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Associado deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Associado was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    void deleteById(@PathVariable(name = "id") String id);
}
