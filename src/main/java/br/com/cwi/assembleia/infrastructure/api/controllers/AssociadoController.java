package br.com.cwi.assembleia.infrastructure.api.controllers;

import br.com.cwi.assembleia.application.associado.create.CreateAssociadoCommand;
import br.com.cwi.assembleia.application.associado.create.CreateAssociadoUseCase;
import br.com.cwi.assembleia.application.associado.delete.DeleteAssociadoUseCase;
import br.com.cwi.assembleia.application.associado.retrieve.get.GetAssociadoByIdUseCase;
import br.com.cwi.assembleia.application.associado.retrieve.list.ListAssociadoUseCase;
import br.com.cwi.assembleia.application.associado.update.UpdateAssociadoCommand;
import br.com.cwi.assembleia.application.associado.update.UpdateAssociadoUseCase;
import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;
import br.com.cwi.assembleia.infrastructure.api.AssociadoAPI;
import br.com.cwi.assembleia.infrastructure.associado.model.AssociadoListResponse;
import br.com.cwi.assembleia.infrastructure.associado.model.AssociadoResponse;
import br.com.cwi.assembleia.infrastructure.associado.model.CreateAssociadoRequest;
import br.com.cwi.assembleia.infrastructure.associado.model.UpdateAssociadoRequest;
import br.com.cwi.assembleia.infrastructure.associado.presenters.AssociadoApiPresenter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
public class AssociadoController implements AssociadoAPI {

    private final CreateAssociadoUseCase createAssociadoUseCase;
    private final DeleteAssociadoUseCase deleteAssociadoUseCase;
    private final GetAssociadoByIdUseCase getAssociadoByIdUseCase;
    private final ListAssociadoUseCase listAssociadoUseCase;
    private final UpdateAssociadoUseCase updateAssociadoUseCase;

    public AssociadoController(CreateAssociadoUseCase createAssociadoUseCase,
                               DeleteAssociadoUseCase deleteAssociadoUseCase,
                               GetAssociadoByIdUseCase getAssociadoByIdUseCase,
                               ListAssociadoUseCase listAssociadoUseCase,
                               UpdateAssociadoUseCase updateAssociadoUseCase) {
        this.createAssociadoUseCase = createAssociadoUseCase;
        this.deleteAssociadoUseCase = deleteAssociadoUseCase;
        this.getAssociadoByIdUseCase = getAssociadoByIdUseCase;
        this.listAssociadoUseCase = listAssociadoUseCase;
        this.updateAssociadoUseCase = updateAssociadoUseCase;
    }

    @Override
    public ResponseEntity<?> create(final CreateAssociadoRequest input) {
        final var command = CreateAssociadoCommand.with(
                input.nome(),
                input.cpf(),
                input.isActive()
        );

        final var output = this.createAssociadoUseCase.execute(command);

        return ResponseEntity.created(URI.create("/associados/" + output.id())).body(output);
    }

    @Override
    public Pagination<AssociadoListResponse> list(
            final String search,
            final int page,
            final int perPage,
            final String sort,
            final String direction
    ) {
        return this.listAssociadoUseCase.execute(new SearchQuery(page, perPage, search, sort, direction))
                .map(AssociadoApiPresenter::present);
    }

    @Override
    public AssociadoResponse getById(final String id) {
        return AssociadoApiPresenter.present(this.getAssociadoByIdUseCase.execute(id));
    }

    @Override
    public ResponseEntity<?> updateById(final String id, final UpdateAssociadoRequest input) {
        final var command = UpdateAssociadoCommand.with(
                id,
                input.nome(),
                input.cpf(),
                input.isActive()
        );

        final var output = this.updateAssociadoUseCase.execute(command);

        return ResponseEntity.ok(output);
    }

    @Override
    public void deleteById(final String id) {
        this.deleteAssociadoUseCase.execute(id);
    }
}
