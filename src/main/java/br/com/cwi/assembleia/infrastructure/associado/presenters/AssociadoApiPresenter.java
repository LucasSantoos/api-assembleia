package br.com.cwi.assembleia.infrastructure.associado.presenters;

import br.com.cwi.assembleia.application.associado.retrieve.get.AssociadoOutput;
import br.com.cwi.assembleia.application.associado.retrieve.list.AssociadoListOutput;
import br.com.cwi.assembleia.infrastructure.associado.model.AssociadoListResponse;
import br.com.cwi.assembleia.infrastructure.associado.model.AssociadoResponse;

public interface AssociadoApiPresenter {

    static AssociadoResponse present(final AssociadoOutput output) {
        return new AssociadoResponse(
                output.id(),
                output.nome(),
                output.cpf(),
                output.isActive(),
                output.createdAt(),
                output.updatedAt(),
                output.deletedAt()
        );
    }

    static AssociadoListResponse present(final AssociadoListOutput output) {
        return new AssociadoListResponse(
                output.id(),
                output.nome(),
                output.cpf(),
                output.isActive(),
                output.createdAt(),
                output.deletedAt()
        );
    }
}
