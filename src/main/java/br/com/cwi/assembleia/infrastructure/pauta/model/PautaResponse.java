package br.com.cwi.assembleia.infrastructure.pauta.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.List;

public record PautaResponse(
        @JsonProperty("id") String id,
        @JsonProperty("descricao") String descricao,
        @JsonProperty("observacao") String observacao,
        @JsonProperty("data_criacao") LocalDateTime dataCriacao,
        @JsonProperty("data_inicio_votacao") LocalDateTime dataInicioVotacao,
        @JsonProperty("data_fim_votacao") LocalDateTime dataFimVotacao,
        @JsonProperty("votos_id") List<String> votos
) {
}
