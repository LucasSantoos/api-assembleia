package br.com.cwi.assembleia.infrastructure.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("br.com.cwi.assembleia")
public class WebServerConfig {
}
