package br.com.cwi.assembleia.infrastructure.pauta.presenters;

import br.com.cwi.assembleia.application.pauta.retrieve.get.PautaOutput;
import br.com.cwi.assembleia.application.pauta.retrieve.list.PautaListOutput;
import br.com.cwi.assembleia.application.pauta.retrieve.result.PautaResultOutput;
import br.com.cwi.assembleia.infrastructure.pauta.model.PautaListResponse;
import br.com.cwi.assembleia.infrastructure.pauta.model.PautaResponse;
import br.com.cwi.assembleia.infrastructure.pauta.model.PautaResultResponse;

public interface PautaApiPresenter {

    static PautaResponse present(final PautaOutput output) {
        return new PautaResponse(
                output.id(),
                output.descricao(),
                output.observacao(),
                output.dataCriacao(),
                output.dataInicioVotacao(),
                output.dataFimVotacao(),
                output.votos()
        );
    }

    static PautaListResponse present(final PautaListOutput output) {
        return new PautaListResponse(
                output.id(),
                output.descricao(),
                output.observacao(),
                output.dataCriacao(),
                output.dataInicioVotacao(),
                output.dataFimVotacao()
        );
    }

    static PautaResultResponse present(final PautaResultOutput output) {
        return new PautaResultResponse(
                output.id(),
                output.descricao(),
                output.observacao(),
                output.result(),
                output.percentage(),
                output.totalVoto()
        );
    }
}
