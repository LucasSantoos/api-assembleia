package br.com.cwi.assembleia.infrastructure.pauta;

import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;
import br.com.cwi.assembleia.domain.pauta.Pauta;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.infrastructure.pauta.persistence.PautaEntity;
import br.com.cwi.assembleia.infrastructure.pauta.persistence.PautaRepository;
import br.com.cwi.assembleia.infrastructure.utils.SpecificationUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.springframework.data.jpa.domain.Specification.where;

@Component
public class PautaSQLiteGateway implements PautaGateway {

    private final PautaRepository pautaRepository;

    public PautaSQLiteGateway(PautaRepository pautaRepository) {
        this.pautaRepository = pautaRepository;
    }

    @Override
    public Pauta create(final Pauta pauta) {
        return save(pauta);
    }

    @Override
    public void deleteById(final PautaID anId) {
        final var pautaId = anId.getValue();
        if (this.pautaRepository.existsById(pautaId)) {
            this.pautaRepository.deleteById(pautaId);
        }
    }

    @Override
    public Optional<Pauta> findById(final PautaID anId) {
        return this.pautaRepository.findById(anId.getValue())
                .map(PautaEntity::toAggregate);
    }

    @Override
    public Pauta update(final Pauta pauta) {
        return save(pauta);
    }

    @Override
    public Pagination<Pauta> findAll(final SearchQuery aQuery) {
        final var page = PageRequest.of(
                aQuery.page(),
                aQuery.perPage(),
                Sort.by(Sort.Direction.fromString(aQuery.direction()), aQuery.sort())
        );

        final var where = Optional.ofNullable(aQuery.terms())
                .filter(str -> !str.isBlank())
                .map(this::assembleSpecification)
                .orElse(null);

        final var pageResult =
                this.pautaRepository.findAll(where(where), page);

        return new Pagination<>(
                pageResult.getNumber(),
                pageResult.getSize(),
                pageResult.getTotalElements(),
                pageResult.map(PautaEntity::toAggregate).toList()
        );
    }

    @Override
    public List<PautaID> existsByIds(final Iterable<PautaID> pautaIDS) {
        final var ids = StreamSupport.stream(pautaIDS.spliterator(), false)
                .map(PautaID::getValue)
                .toList();
        return this.pautaRepository.existsByIds(ids).stream()
                .map(PautaID::from)
                .toList();
    }

    private Pauta save(final Pauta pauta) {
        return this.pautaRepository.save(PautaEntity.from(pauta))
                .toAggregate();
    }

    private Specification<PautaEntity> assembleSpecification(final String terms) {
        return SpecificationUtils.like("descricao", terms);
    }

    @Override
    public Optional<PautaEntity> getByPautaCodigoAndBetweenSessao(String id, LocalDateTime data) {
        return this.pautaRepository.getByPautaCodigoAndBetweenSessao(id, data);
    }
}
