package br.com.cwi.assembleia.infrastructure.pauta.persistence;

import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.pauta.Pauta;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.voto.Voto;
import br.com.cwi.assembleia.domain.voto.VotoID;
import br.com.cwi.assembleia.infrastructure.voto.persistence.VotoEntity;
import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "pauta")
public class PautaEntity {

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "data_criacao")
    private LocalDateTime dataCriacao;

    @Column(name = "data_inicio_votacao")
    private LocalDateTime dataInicioVotacao;

    @Column(name = "data_fim_votacao")
    private LocalDateTime dataFimVotacao;

    @OneToMany(mappedBy = "pauta", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<VotoEntity> votos = new ArrayList<>();

    public PautaEntity() {
    }

    public PautaEntity(String id, String descricao, String observacao, LocalDateTime dataCriacao, LocalDateTime dataInicioVotacao, LocalDateTime dataFimVotacao) {
        this.id = id;
        this.descricao = descricao;
        this.observacao = observacao;
        this.dataCriacao = dataCriacao;
        this.dataInicioVotacao = dataInicioVotacao;
        this.dataFimVotacao = dataFimVotacao;
    }

    public static PautaEntity from(final Pauta pauta) {
        final var entity = new PautaEntity(
                pauta.getId().getValue(),
                pauta.getDescricao(),
                pauta.getObservacao(),
                pauta.getDataCriacao(),
                pauta.getDataInicioVotacao(),
                pauta.getDataFimVotacao()
        );
        pauta.getVotos()
                .forEach(entity::addVoto);
        return entity;
    }

    public static PautaEntity from(final PautaID pautaID) {
        final var entity = new PautaEntity();
        entity.setId(pautaID.getValue());
        return entity;
    }

    public Pauta toAggregate() {
        return Pauta.with(
                PautaID.from(getId()),
                getDescricao(),
                getObservacao(),
                getDataCriacao(),
                getDataInicioVotacao(),
                getDataFimVotacao(),
                getVotoIDs()
        );
    }

    public List<Voto> getVotoIDs() {
        return getVotos().stream()
                .map(it -> Voto.with(VotoID.from(it.getId()),
                        AssociadoID.from(it.getAssociado().getId()),
                        PautaID.from(it.getPauta().getId()),
                        it.getOpcao(),
                        it.getDataCriacao()
                ))
                .toList();
    }

    public List<VotoEntity> getVotos() {
        return votos;
    }

    public PautaEntity setVotos(List<VotoEntity> votos) {
        this.votos = votos;
        return this;
    }

    private void addVoto(final Voto voto) {
        this.votos.add(VotoEntity.from(voto));
    }

    private void removeVotos(final VotoID id) {
        this.votos.remove(VotoEntity.from(this, id));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public LocalDateTime getDataInicioVotacao() {
        return dataInicioVotacao;
    }

    public void setDataInicioVotacao(LocalDateTime dataInicioVotacao) {
        this.dataInicioVotacao = dataInicioVotacao;
    }

    public LocalDateTime getDataFimVotacao() {
        return dataFimVotacao;
    }

    public void setDataFimVotacao(LocalDateTime dataFimVotacao) {
        this.dataFimVotacao = dataFimVotacao;
    }
}
