package br.com.cwi.assembleia.infrastructure.associado.persistence;

import br.com.cwi.assembleia.domain.associado.Associado;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.time.Instant;


@Entity(name = "associado")
public class AssociadoEntity {

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "nome", nullable = false)
    private String nome;

    @Column(name = "cpf", nullable = false)
    private String cpf;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "created_at", nullable = false, columnDefinition = "DATETIME(6)")
    private Instant createdAt;

    @Column(name = "updated_at", nullable = false, columnDefinition = "DATETIME(6)")
    private Instant updatedAt;

    @Column(name = "deleted_at", columnDefinition = "DATETIME(6)")
    private Instant deletedAt;

    public AssociadoEntity() {
    }

    public AssociadoEntity(String id, String nome, String cpf, boolean active, Instant createdAt, Instant updatedAt, Instant deletedAt) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.active = active;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    public static AssociadoEntity from(final Associado associado) {
        final var entity = new AssociadoEntity(
                associado.getId().getValue(),
                associado.getNome(),
                associado.getCpf(),
                associado.isActive(),
                associado.getCreatedAt(),
                associado.getUpdatedAt(),
                associado.getDeletedAt()
        );
        return entity;
    }

    public static AssociadoEntity from(final AssociadoID associadoID) {
        final var entity = new AssociadoEntity();
        entity.setId(associadoID.getValue());
        return entity;
    }

    public Associado toAggregate() {
        return Associado.with(
                AssociadoID.from(getId()),
                getNome(),
                getCpf(),
                isActive(),
                getCreatedAt(),
                getUpdatedAt(),
                getDeletedAt()
        );
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Instant getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Instant deletedAt) {
        this.deletedAt = deletedAt;
    }
}
