package br.com.cwi.assembleia.infrastructure.associado;

import br.com.cwi.assembleia.domain.associado.Associado;
import br.com.cwi.assembleia.domain.associado.AssociadoGateway;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;
import br.com.cwi.assembleia.infrastructure.associado.persistence.AssociadoEntity;
import br.com.cwi.assembleia.infrastructure.associado.persistence.AssociadoRepository;
import br.com.cwi.assembleia.infrastructure.utils.SpecificationUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.springframework.data.jpa.domain.Specification.where;

@Component
public class AssociadoSQLiteGateway implements AssociadoGateway {

    private final AssociadoRepository associadoRepository;

    public AssociadoSQLiteGateway(AssociadoRepository associadoRepository) {
        this.associadoRepository = associadoRepository;
    }

    @Override
    public Associado create(final Associado aAssociado) {
        return save(aAssociado);
    }

    @Override
    public void deleteById(final AssociadoID anId) {
        final var associadoId = anId.getValue();
        if (this.associadoRepository.existsById(associadoId)) {
            this.associadoRepository.deleteById(associadoId);
        }
    }

    @Override
    public Optional<Associado> findById(final AssociadoID anId) {
        return this.associadoRepository.findById(anId.getValue())
                .map(AssociadoEntity::toAggregate);
    }

    @Override
    public Associado update(final Associado aAssociado) {
        return save(aAssociado);
    }

    @Override
    public Pagination<Associado> findAll(final SearchQuery aQuery) {
        final var page = PageRequest.of(
                aQuery.page(),
                aQuery.perPage(),
                Sort.by(Sort.Direction.fromString(aQuery.direction()), aQuery.sort())
        );

        final var where = Optional.ofNullable(aQuery.terms())
                .filter(str -> !str.isBlank())
                .map(this::assembleSpecification)
                .orElse(null);

        final var pageResult =
                this.associadoRepository.findAll(where(where), page);

        return new Pagination<>(
                pageResult.getNumber(),
                pageResult.getSize(),
                pageResult.getTotalElements(),
                pageResult.map(AssociadoEntity::toAggregate).toList()
        );
    }

    @Override
    public List<AssociadoID> existsByIds(final Iterable<AssociadoID> associadoIDS) {
        final var ids = StreamSupport.stream(associadoIDS.spliterator(), false)
                .map(AssociadoID::getValue)
                .toList();
        return this.associadoRepository.existsByIds(ids).stream()
                .map(AssociadoID::from)
                .toList();
    }

    private Associado save(final Associado aAssociado) {
        return this.associadoRepository.save(AssociadoEntity.from(aAssociado))
                .toAggregate();
    }

    private Specification<AssociadoEntity> assembleSpecification(final String terms) {
        return SpecificationUtils.like("nome", terms);
    }
}
