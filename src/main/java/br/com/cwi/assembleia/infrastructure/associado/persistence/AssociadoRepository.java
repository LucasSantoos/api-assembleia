package br.com.cwi.assembleia.infrastructure.associado.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AssociadoRepository extends JpaRepository<AssociadoEntity, String> {

    Page<AssociadoEntity> findAll(Specification<AssociadoEntity> whereClause, Pageable page);

    @Query(value = "select a.id from associado a where a.id in :ids")
    List<String> existsByIds(@Param("ids") List<String> ids);
}
