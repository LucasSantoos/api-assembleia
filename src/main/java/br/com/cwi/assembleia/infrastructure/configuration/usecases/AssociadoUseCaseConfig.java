package br.com.cwi.assembleia.infrastructure.configuration.usecases;

import br.com.cwi.assembleia.application.associado.create.CreateAssociadoUseCase;
import br.com.cwi.assembleia.application.associado.delete.DeleteAssociadoUseCase;
import br.com.cwi.assembleia.application.associado.retrieve.get.GetAssociadoByIdUseCase;
import br.com.cwi.assembleia.application.associado.retrieve.list.ListAssociadoUseCase;
import br.com.cwi.assembleia.application.associado.update.UpdateAssociadoUseCase;
import br.com.cwi.assembleia.domain.associado.AssociadoGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AssociadoUseCaseConfig {

    private final AssociadoGateway associadoGateway;

    public AssociadoUseCaseConfig(final AssociadoGateway associadoGateway) {
        this.associadoGateway = associadoGateway;
    }

    @Bean
    public CreateAssociadoUseCase createAssociadoUseCase() {
        return new CreateAssociadoUseCase(associadoGateway);
    }

    @Bean
    public DeleteAssociadoUseCase deleteAssociadoUseCase() {
        return new DeleteAssociadoUseCase(associadoGateway);
    }

    @Bean
    public GetAssociadoByIdUseCase getAssociadoByIdUseCase() {
        return new GetAssociadoByIdUseCase(associadoGateway);
    }

    @Bean
    public ListAssociadoUseCase listAssociadoUseCase() {
        return new ListAssociadoUseCase(associadoGateway);
    }

    @Bean
    public UpdateAssociadoUseCase updateAssociadoUseCase() {
        return new UpdateAssociadoUseCase(associadoGateway);
    }
}
