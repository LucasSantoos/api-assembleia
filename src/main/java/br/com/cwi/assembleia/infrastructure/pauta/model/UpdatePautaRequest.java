package br.com.cwi.assembleia.infrastructure.pauta.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public record UpdatePautaRequest(
        @JsonProperty("descricao") String descricao,
        @JsonProperty("observacao") String observacao,
        @JsonProperty("data_inicio_votacao") LocalDateTime dataInicioVotacao,
        @JsonProperty("data_fim_votacao") LocalDateTime dataFimVotacao,
        @JsonProperty("votos_id") List<String> votos
) {
    public List<String> votos() {
        return this.votos != null ? this.votos : Collections.emptyList();
    }
}
