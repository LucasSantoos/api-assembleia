package br.com.cwi.assembleia.infrastructure.api;

import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.infrastructure.pauta.model.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "pautas")
public interface PautaAPI {

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Create a new pauta")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created successfully"),
            @ApiResponse(responseCode = "422", description = "A validation error was thrown"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    ResponseEntity<?> create(@RequestBody CreatePautaRequest input);

    @GetMapping
    @Operation(summary = "List all pautas paginated")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Listed successfully"),
            @ApiResponse(responseCode = "422", description = "A invalid parameter was received"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    Pagination<PautaListResponse> list(
            @RequestParam(name = "search", required = false, defaultValue = "") final String search,
            @RequestParam(name = "page", required = false, defaultValue = "0") final int page,
            @RequestParam(name = "perPage", required = false, defaultValue = "10") final int perPage,
            @RequestParam(name = "sort", required = false, defaultValue = "descricao") final String sort,
            @RequestParam(name = "dir", required = false, defaultValue = "asc") final String direction
    );

    @GetMapping(
            value = "{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Get a pauta by it's identifier")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pauta retrieved successfully"),
            @ApiResponse(responseCode = "404", description = "Pauta was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    PautaResponse getById(@PathVariable(name = "id") String id);

    @GetMapping(
            value = "{id}/result",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Get a result pauta by it's identifier")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pauta result retrieved successfully"),
            @ApiResponse(responseCode = "404", description = "Pauta result was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    PautaResultResponse getByResultId(@PathVariable(name = "id") String id);

    @PostMapping(
            value = "{id}/start",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Start voting pauta")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pauta start successfully"),
            @ApiResponse(responseCode = "404", description = "Pauta result was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    ResponseEntity<?> start(@PathVariable(name = "id") String id);

    @PostMapping(
            value = "{id}/start/{minutes}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Start with minutes voting pauta")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pauta start successfully"),
            @ApiResponse(responseCode = "404", description = "Pauta result was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    ResponseEntity<?> startWithMinutes(@PathVariable(name = "id") String id, @PathVariable(name = "minutes") Long minutes);

    @PutMapping(
            value = "{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @Operation(summary = "Update a pauta by it's identifier")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pauta updated successfully"),
            @ApiResponse(responseCode = "404", description = "Pauta was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    ResponseEntity<?> updateById(@PathVariable(name = "id") String id, @RequestBody UpdatePautaRequest input);

    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete a pauta by it's identifier")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Pauta deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Pauta was not found"),
            @ApiResponse(responseCode = "500", description = "An internal server error was thrown"),
    })
    void deleteById(@PathVariable(name = "id") String id);
}
