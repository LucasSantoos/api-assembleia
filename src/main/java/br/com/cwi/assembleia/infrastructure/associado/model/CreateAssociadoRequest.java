package br.com.cwi.assembleia.infrastructure.associado.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CreateAssociadoRequest(
        @JsonProperty("nome") String nome,
        @JsonProperty("cpf") String cpf,
        @JsonProperty("is_active") Boolean active
) {
    public boolean isActive() {
        return this.active != null ? this.active : true;
    }
}
