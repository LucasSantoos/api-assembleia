package br.com.cwi.assembleia.infrastructure.configuration.exception;

import br.com.cwi.assembleia.domain.exceptions.DomainException;
import br.com.cwi.assembleia.domain.exceptions.NotFoundException;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.validation.Error;
import br.com.cwi.assembleia.infrastructure.configuration.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandlerConfig {

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleNotificationException(NotificationException e) {
        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST.value(),
                e.getMessage(),
                e.getErrors().stream().map(Error::message).collect(Collectors.toList()),
                LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDomainException(DomainException e) {
        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST.value(),
                e.getMessage(),
                e.getErrors().stream().map(Error::message).collect(Collectors.toList()),
                LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleNotFoundException(NotFoundException e) {
        ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.value(),
                e.getMessage(),
                e.getErrors().stream().map(Error::message).collect(Collectors.toList()),
                LocalDateTime.now());
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}
