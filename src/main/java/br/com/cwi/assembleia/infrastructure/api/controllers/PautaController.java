package br.com.cwi.assembleia.infrastructure.api.controllers;

import br.com.cwi.assembleia.application.pauta.create.CreatePautaCommand;
import br.com.cwi.assembleia.application.pauta.create.CreatePautaUseCase;
import br.com.cwi.assembleia.application.pauta.delete.DeletePautaUseCase;
import br.com.cwi.assembleia.application.pauta.retrieve.get.GetPautaByIdUseCase;
import br.com.cwi.assembleia.application.pauta.retrieve.list.ListPautaUseCase;
import br.com.cwi.assembleia.application.pauta.retrieve.result.ResultPautaUseCase;
import br.com.cwi.assembleia.application.pauta.start.StartPautaCommand;
import br.com.cwi.assembleia.application.pauta.start.StartPautaUseCase;
import br.com.cwi.assembleia.application.pauta.update.UpdatePautaCommand;
import br.com.cwi.assembleia.application.pauta.update.UpdatePautaUseCase;
import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;
import br.com.cwi.assembleia.infrastructure.api.PautaAPI;
import br.com.cwi.assembleia.infrastructure.pauta.model.*;
import br.com.cwi.assembleia.infrastructure.pauta.presenters.PautaApiPresenter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
public class PautaController implements PautaAPI {

    private final CreatePautaUseCase createPautaUseCase;
    private final DeletePautaUseCase deletePautaUseCase;
    private final GetPautaByIdUseCase getPautaByIdUseCase;
    private final ListPautaUseCase listPautaUseCase;
    private final UpdatePautaUseCase updatePautaUseCase;
    private final ResultPautaUseCase resultPautaUseCase;
    private final StartPautaUseCase startPautaUseCase;

    public PautaController(CreatePautaUseCase createPautaUseCase,
                           DeletePautaUseCase deletePautaUseCase,
                           GetPautaByIdUseCase getPautaByIdUseCase,
                           ListPautaUseCase listPautaUseCase,
                           UpdatePautaUseCase updatePautaUseCase,
                           ResultPautaUseCase resultPautaUseCase,
                           StartPautaUseCase startPautaUseCase
    ) {
        this.createPautaUseCase = createPautaUseCase;
        this.deletePautaUseCase = deletePautaUseCase;
        this.getPautaByIdUseCase = getPautaByIdUseCase;
        this.listPautaUseCase = listPautaUseCase;
        this.updatePautaUseCase = updatePautaUseCase;
        this.resultPautaUseCase = resultPautaUseCase;
        this.startPautaUseCase = startPautaUseCase;
    }

    @Override
    public ResponseEntity<?> create(final CreatePautaRequest input) {
        final var command = CreatePautaCommand.with(
                input.descricao(),
                input.observacao(),
                input.dataInicioVotacao(),
                input.dataFimVotacao()
        );

        final var output = this.createPautaUseCase.execute(command);

        return ResponseEntity.created(URI.create("/pautas/" + output.id())).body(output);
    }

    @Override
    public Pagination<PautaListResponse> list(
            final String search,
            final int page,
            final int perPage,
            final String sort,
            final String direction
    ) {
        return this.listPautaUseCase.execute(new SearchQuery(page, perPage, search, sort, direction))
                .map(PautaApiPresenter::present);
    }

    @Override
    public PautaResponse getById(final String id) {
        return PautaApiPresenter.present(this.getPautaByIdUseCase.execute(id));
    }

    @Override
    public ResponseEntity<?> updateById(final String id, final UpdatePautaRequest input) {
        final var command = UpdatePautaCommand.with(
                id,
                input.descricao(),
                input.observacao()
        );

        final var output = this.updatePautaUseCase.execute(command);

        return ResponseEntity.ok(output);
    }

    @Override
    public void deleteById(final String id) {
        this.deletePautaUseCase.execute(id);
    }

    @Override
    public PautaResultResponse getByResultId(String id) {
        return PautaApiPresenter.present(this.resultPautaUseCase.execute(id));
    }

    @Override
    public ResponseEntity<?> start(String id) {
        final var command = StartPautaCommand.with(
                id,
                1L
        );
        final var output = this.startPautaUseCase.execute(command);
        return ResponseEntity.ok(output);
    }

    @Override
    public ResponseEntity<?> startWithMinutes(String id, Long minutes) {
        final var command = StartPautaCommand.with(
                id,
                minutes
        );
        final var output = this.startPautaUseCase.execute(command);
        return ResponseEntity.ok(output);
    }
}
