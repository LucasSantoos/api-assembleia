package br.com.cwi.assembleia.infrastructure.pauta.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PautaRepository extends JpaRepository<PautaEntity, String> {

    Page<PautaEntity> findAll(Specification<PautaEntity> whereClause, Pageable page);

    @Query(value = "select a.id from pauta a where a.id in :ids")
    List<String> existsByIds(@Param("ids") List<String> ids);

    @Query("select p from pauta p where p.id = ?1 and  p.dataInicioVotacao <= ?2 and p.dataFimVotacao >= ?2")
    Optional<PautaEntity> getByPautaCodigoAndBetweenSessao(String id, LocalDateTime data);
}
