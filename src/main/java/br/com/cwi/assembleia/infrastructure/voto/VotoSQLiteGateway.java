package br.com.cwi.assembleia.infrastructure.voto;

import br.com.cwi.assembleia.domain.pagination.Pagination;
import br.com.cwi.assembleia.domain.pagination.SearchQuery;
import br.com.cwi.assembleia.domain.voto.Voto;
import br.com.cwi.assembleia.domain.voto.VotoGateway;
import br.com.cwi.assembleia.domain.voto.VotoID;
import br.com.cwi.assembleia.infrastructure.utils.SpecificationUtils;
import br.com.cwi.assembleia.infrastructure.voto.persistence.VotoEntity;
import br.com.cwi.assembleia.infrastructure.voto.persistence.VotoRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.springframework.data.jpa.domain.Specification.where;

@Component
public class VotoSQLiteGateway implements VotoGateway {

    private final VotoRepository votoRepository;

    public VotoSQLiteGateway(VotoRepository votoRepository) {
        this.votoRepository = votoRepository;
    }

    @Override
    public Voto create(final Voto voto) {
        return save(voto);
    }

    @Override
    public void deleteById(final VotoID anId) {
        final var associadoId = anId.getValue();
        if (this.votoRepository.existsById(associadoId)) {
            this.votoRepository.deleteById(associadoId);
        }
    }

    @Override
    public Optional<Voto> findById(final VotoID anId) {
        return this.votoRepository.findById(anId.getValue())
                .map(VotoEntity::toAggregate);
    }

    @Override
    public Voto update(final Voto voto) {
        return save(voto);
    }

    @Override
    public Pagination<Voto> findAll(final SearchQuery aQuery) {
        final var page = PageRequest.of(
                aQuery.page(),
                aQuery.perPage(),
                Sort.by(Sort.Direction.fromString(aQuery.direction()), aQuery.sort())
        );

        final var where = Optional.ofNullable(aQuery.terms())
                .filter(str -> !str.isBlank())
                .map(this::assembleSpecification)
                .orElse(null);

        final var pageResult =
                this.votoRepository.findAll(where(where), page);

        return new Pagination<>(
                pageResult.getNumber(),
                pageResult.getSize(),
                pageResult.getTotalElements(),
                pageResult.map(VotoEntity::toAggregate).toList()
        );
    }

    @Override
    public List<VotoID> existsByIds(final Iterable<VotoID> associadoIDS) {
        final var ids = StreamSupport.stream(associadoIDS.spliterator(), false)
                .map(VotoID::getValue)
                .toList();
        return this.votoRepository.existsByIds(ids).stream()
                .map(VotoID::from)
                .toList();
    }

    private Voto save(final Voto voto) {
        return this.votoRepository.save(VotoEntity.from(voto))
                .toAggregate();
    }

    private Specification<VotoEntity> assembleSpecification(final String terms) {
        return SpecificationUtils.like("nome", terms);
    }

    @Override
    public List<String> existsByAssociadoAndPautaId(String associadoID, String pautaID) {
        return this.votoRepository.existsByAssociadoAndPautaId(associadoID, pautaID);
    }
}
