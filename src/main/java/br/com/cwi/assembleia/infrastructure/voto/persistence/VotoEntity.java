package br.com.cwi.assembleia.infrastructure.voto.persistence;

import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.voto.Voto;
import br.com.cwi.assembleia.domain.voto.VotoID;
import br.com.cwi.assembleia.infrastructure.associado.persistence.AssociadoEntity;
import br.com.cwi.assembleia.infrastructure.pauta.persistence.PautaEntity;
import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity(name = "voto")
public class VotoEntity {

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "opcao")
    private Boolean opcao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pauta_id")
    private PautaEntity pauta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "associado_id")
    private AssociadoEntity associado;

    @Column(name = "data_criacao")
    private LocalDateTime dataCriacao;

    public VotoEntity() {
    }

    private VotoEntity(String id, Boolean opcao, PautaEntity pauta, AssociadoEntity associado, LocalDateTime dataCriacao) {
        this.id = id;
        this.opcao = opcao;
        this.pauta = pauta;
        this.associado = associado;
        this.dataCriacao = dataCriacao;
    }

    public static VotoEntity from(final PautaEntity pauta, final VotoID votoID) {
        return new VotoEntity(

        );
    }

    public static VotoEntity from(final Voto voto) {
        return new VotoEntity(
                voto.getId().getValue(),
                voto.getOpcao(),
                PautaEntity.from(voto.getPautaID()),
                AssociadoEntity.from(voto.getAssociadoID()),
                voto.getDataCriacao()
        );
    }

    public Voto toAggregate() {
        return Voto.with(
                VotoID.from(getId()),
                AssociadoID.from(getAssociado().getId()),
                PautaID.from(getPauta().getId()),
                getOpcao(),
                getDataCriacao()
        );
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getOpcao() {
        return opcao;
    }

    public LocalDateTime getDataCriacao() {
        return dataCriacao;
    }

    public PautaEntity getPauta() {
        return pauta;
    }

    public AssociadoEntity getAssociado() {
        return associado;
    }
}
