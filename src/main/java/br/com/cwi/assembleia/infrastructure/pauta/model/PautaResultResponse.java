package br.com.cwi.assembleia.infrastructure.pauta.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public record PautaResultResponse(
        @JsonProperty("id") String id,
        @JsonProperty("descricao") String descricao,
        @JsonProperty("observacao") String observacao,
        @JsonProperty("result") Map<Boolean, Long> result,
        @JsonProperty("percentage") Map<Boolean, Long> percentage,
        @JsonProperty("totalVotos") Long totalVotos
) {
}
