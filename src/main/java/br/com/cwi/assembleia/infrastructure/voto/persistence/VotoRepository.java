package br.com.cwi.assembleia.infrastructure.voto.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VotoRepository extends JpaRepository<VotoEntity, String> {

    Page<VotoEntity> findAll(Specification<VotoEntity> whereClause, Pageable page);

    @Query(value = "select a.id from voto a where a.id in :ids")
    List<String> existsByIds(@Param("ids") List<String> ids);

    @Query(nativeQuery = true, value = "select v.id from voto v where v.associado_id = :associadoID and v.pauta_id = :pautaID")
    List<String> existsByAssociadoAndPautaId(String associadoID, String pautaID);
}
