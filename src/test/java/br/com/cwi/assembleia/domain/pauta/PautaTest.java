package br.com.cwi.assembleia.domain.pauta;

import br.com.cwi.assembleia.domain.UnitTest;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PautaTest extends UnitTest {

    @Test
    public void givenValidParams_whenCallNewPauta_shouldInstantiateAPauta() {
        final var expectedDescricao = "Descrição";
        final var expectedObservacao = "Observação";
        final var expectedVotos = 0;

        final var actualPauta = Pauta.newPauta(expectedDescricao, expectedObservacao);

        Assertions.assertNotNull(actualPauta);
        Assertions.assertNotNull(actualPauta.getId());
        Assertions.assertEquals(expectedDescricao, actualPauta.getDescricao());
        Assertions.assertEquals(expectedObservacao, actualPauta.getObservacao());
        Assertions.assertEquals(expectedVotos, actualPauta.getVotos().size());
    }

    @Test
    public void givenInvalidNullName_whenCallNewPautaAndValidate_shouldReceiveAError() {
        final String expectedDescricao = null;
        final var expectedObservacao = "Observação";
        final var expectedErrorCount = 1;

        final var expectedErrorMessage = "'descricao' should not be null";

        final var actualException = Assertions.assertThrows(NotificationException.class, () -> {
            Pauta.newPauta(expectedDescricao, expectedObservacao);
        });

        Assertions.assertEquals(expectedErrorCount, actualException.getErrors().size());
        Assertions.assertEquals(expectedErrorMessage, actualException.getErrors().get(0).message());
    }

}