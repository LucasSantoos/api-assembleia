package br.com.cwi.assembleia.domain.voto;

import br.com.cwi.assembleia.domain.UnitTest;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class VotoTest extends UnitTest {

    @Test
    public void givenValidParams_whenCallNewVoto_shouldInstantiateAVoto() {
        final var expectedAssociadoID = AssociadoID.unique();
        final var expectedPautaID = PautaID.unique();
        final var expectedOpcao = true;

        final var actualVoto = Voto.newVoto(expectedAssociadoID, expectedPautaID, expectedOpcao);

        Assertions.assertNotNull(actualVoto);
        Assertions.assertNotNull(actualVoto.getId());
        Assertions.assertEquals(expectedAssociadoID, actualVoto.getAssociadoID());
        Assertions.assertEquals(expectedPautaID, actualVoto.getPautaID());
        Assertions.assertEquals(expectedOpcao, actualVoto.getOpcao());
    }
}