package br.com.cwi.assembleia.application.pauta.retrieve.result;

import br.com.cwi.assembleia.application.UseCaseTest;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.pauta.Pauta;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.voto.Voto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ResultPautaUseCaseTest extends UseCaseTest {

    @InjectMocks
    private ResultPautaUseCase useCase;

    @Mock
    private PautaGateway pautaGateway;

    @Override
    protected List<Object> getMocks() {
        return List.of(pautaGateway);
    }

    @Test
    public void givenAValidCommand_whenCallsResultPauta_shouldReturnResult() {
        // given
        final var expectedPautaID = PautaID.unique();
        final var expectedDescricao = "Descrição";
        final var expectedObservacao = "Observação";

        final var expectedVotos = List.of(
                Voto.newVoto(AssociadoID.unique(), expectedPautaID, Boolean.TRUE),
                Voto.newVoto(AssociadoID.unique(), expectedPautaID, Boolean.FALSE)
        );

        final var expectedTotalVoto = 2L;
        var expectedResult = new HashMap<>();
        expectedResult.put(false, 1L);
        expectedResult.put(true, 1L);
        var expectedPercentage = new HashMap<>();
        expectedPercentage.put(false, 50L);
        expectedPercentage.put(true, 50L);

        final var command = expectedPautaID.getValue();

        final var pauta = Pauta.newPauta(expectedDescricao, expectedObservacao)
                .addVotos(expectedVotos);

        final var expectedId = pauta.getId();

        when(pautaGateway.findById(any(PautaID.class))).thenReturn(Optional.of(pauta));

        // when
        final var actualPauta = useCase.execute(command);

        // then
        Assertions.assertEquals(expectedId.getValue(), actualPauta.id());
        Assertions.assertEquals(expectedDescricao, actualPauta.descricao());
        Assertions.assertEquals(expectedObservacao, actualPauta.observacao());
        Assertions.assertEquals(expectedTotalVoto, actualPauta.totalVoto());
        Assertions.assertEquals(expectedResult, actualPauta.result());
        Assertions.assertEquals(expectedPercentage, actualPauta.percentage());
    }

    @Test
    public void givenAValidCommandAllTrue_whenCallsResultPauta_shouldReturnResult() {
        // given
        final var expectedPautaID = PautaID.unique();
        final var expectedDescricao = "Descrição";
        final var expectedObservacao = "Observação";

        final var expectedVotos = List.of(
                Voto.newVoto(AssociadoID.unique(), expectedPautaID, Boolean.TRUE),
                Voto.newVoto(AssociadoID.unique(), expectedPautaID, Boolean.TRUE)
        );

        final var expectedTotalVoto = 2L;
        var expectedResult = new HashMap<>();
        expectedResult.put(true, 2L);
        var expectedPercentage = new HashMap<>();
        expectedPercentage.put(true, 100L);

        final var command = expectedPautaID.getValue();

        final var pauta = Pauta.newPauta(expectedDescricao, expectedObservacao)
                .addVotos(expectedVotos);

        final var expectedId = pauta.getId();

        when(pautaGateway.findById(any(PautaID.class))).thenReturn(Optional.of(pauta));

        // when
        final var actualPauta = useCase.execute(command);

        // then
        Assertions.assertEquals(expectedId.getValue(), actualPauta.id());
        Assertions.assertEquals(expectedDescricao, actualPauta.descricao());
        Assertions.assertEquals(expectedObservacao, actualPauta.observacao());
        Assertions.assertEquals(expectedTotalVoto, actualPauta.totalVoto());
        Assertions.assertEquals(expectedResult, actualPauta.result());
        Assertions.assertEquals(expectedPercentage, actualPauta.percentage());
    }

    @Test
    public void givenAValidCommandAllFalse_whenCallsResultPauta_shouldReturnResult() {
        // given
        final var expectedPautaID = PautaID.unique();
        final var expectedDescricao = "Descrição";
        final var expectedObservacao = "Observação";

        final var expectedVotos = List.of(
                Voto.newVoto(AssociadoID.unique(), expectedPautaID, Boolean.FALSE),
                Voto.newVoto(AssociadoID.unique(), expectedPautaID, Boolean.FALSE)
        );

        final var expectedTotalVoto = 2L;
        var expectedResult = new HashMap<>();
        expectedResult.put(false, 2L);
        var expectedPercentage = new HashMap<>();
        expectedPercentage.put(false, 100L);

        final var command = expectedPautaID.getValue();

        final var pauta = Pauta.newPauta(expectedDescricao, expectedObservacao)
                .addVotos(expectedVotos);

        final var expectedId = pauta.getId();

        when(pautaGateway.findById(any(PautaID.class))).thenReturn(Optional.of(pauta));

        // when
        final var actualPauta = useCase.execute(command);

        // then
        Assertions.assertEquals(expectedId.getValue(), actualPauta.id());
        Assertions.assertEquals(expectedDescricao, actualPauta.descricao());
        Assertions.assertEquals(expectedObservacao, actualPauta.observacao());
        Assertions.assertEquals(expectedTotalVoto, actualPauta.totalVoto());
        Assertions.assertEquals(expectedResult, actualPauta.result());
        Assertions.assertEquals(expectedPercentage, actualPauta.percentage());
    }
}