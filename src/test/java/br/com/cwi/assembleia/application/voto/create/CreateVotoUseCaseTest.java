package br.com.cwi.assembleia.application.voto.create;

import br.com.cwi.assembleia.application.UseCaseTest;
import br.com.cwi.assembleia.domain.associado.AssociadoID;
import br.com.cwi.assembleia.domain.exceptions.NotificationException;
import br.com.cwi.assembleia.domain.pauta.PautaGateway;
import br.com.cwi.assembleia.domain.pauta.PautaID;
import br.com.cwi.assembleia.domain.voto.VotoGateway;
import br.com.cwi.assembleia.infrastructure.pauta.persistence.PautaEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

public class CreateVotoUseCaseTest extends UseCaseTest {

    @InjectMocks
    private CreateVotoUseCase useCase;

    @Mock
    private PautaGateway pautaGateway;

    @Mock
    private VotoGateway votoGateway;

    @Override
    protected List<Object> getMocks() {
        return List.of(pautaGateway, votoGateway);
    }

    @Test
    public void givenAValidCommand_whenCallsCreateVoto_shouldReturnVotoId() {
        // given
        final var expectAssociadoID = AssociadoID.unique();
        final var expectedPautaID = PautaID.unique();
        final var expectedOpcao = true;

        final var command =
                CreateVotoCommand.with(expectAssociadoID.getValue(), expectedPautaID.getValue(), expectedOpcao);

        when(pautaGateway.getByPautaCodigoAndBetweenSessao(any(), any()))
                .thenReturn(Optional.of(new PautaEntity()));

        when(votoGateway.existsByAssociadoAndPautaId(any(), any()))
                .thenReturn(Collections.emptyList());

        when(votoGateway.create(any()))
                .thenAnswer(returnsFirstArg());

        // when
        final var actualOutput = useCase.execute(command);

        // then
        Assertions.assertNotNull(actualOutput);
        Assertions.assertNotNull(actualOutput.id());

        Mockito.verify(votoGateway, times(1)).create(argThat(voto ->
                Objects.equals(expectAssociadoID, voto.getAssociadoID())
                        && Objects.equals(expectedPautaID, voto.getPautaID())
                        && Objects.equals(expectedOpcao, voto.getOpcao())
                        && Objects.nonNull(voto.getId())
        ));
    }

    @Test
    public void givenAInvalidCommand_whenCallsCreateVoto_shouldDomainException() {
        // given
        final var expectAssociadoID = AssociadoID.unique();
        final var expectedPautaID = PautaID.unique();
        final var expectedOpcao = true;

        final var expectedErrorMessage = "There is already a vote registered for this";
        final var expectedErrorCount = 1;

        final var command =
                CreateVotoCommand.with(expectAssociadoID.getValue(), expectedPautaID.getValue(), expectedOpcao);

        when(pautaGateway.getByPautaCodigoAndBetweenSessao(any(), any()))
                .thenReturn(Optional.of(new PautaEntity()));

        when(votoGateway.existsByAssociadoAndPautaId(any(), any()))
                .thenReturn(List.of("Voto já cadastrado"));

        // when
        final var actualException = Assertions.assertThrows(NotificationException.class, () -> {
            useCase.execute(command);
        });

        // then
        Assertions.assertNotNull(actualException);

        Assertions.assertEquals(expectedErrorCount, actualException.getErrors().size());
        Assertions.assertEquals(expectedErrorMessage, actualException.getErrors().get(0).message());

        Mockito.verify(votoGateway, times(1)).existsByAssociadoAndPautaId(any(), any());
        Mockito.verify(votoGateway, times(0)).create(any());
    }

    @Test
    public void givenAInvalidCommand_whenCallsCreateVoto_shouldDomainException2() {
        // given
        final var expectAssociadoID = AssociadoID.unique();
        final var expectedPautaID = PautaID.unique();
        final var expectedOpcao = true;

        final var expectedErrorMessage = "Pauta it's unavailable";
        final var expectedErrorCount = 1;

        final var command =
                CreateVotoCommand.with(expectAssociadoID.getValue(), expectedPautaID.getValue(), expectedOpcao);

        when(pautaGateway.getByPautaCodigoAndBetweenSessao(any(), any()))
                .thenReturn(Optional.empty());

        // when
        final var actualException = Assertions.assertThrows(NotificationException.class, () -> {
            useCase.execute(command);
        });

        // then
        Assertions.assertNotNull(actualException);

        Assertions.assertEquals(expectedErrorCount, actualException.getErrors().size());
        Assertions.assertEquals(expectedErrorMessage, actualException.getErrors().get(0).message());

        Mockito.verify(pautaGateway, times(1)).getByPautaCodigoAndBetweenSessao(any(), any());
        Mockito.verify(votoGateway, times(0)).create(any());
    }
}