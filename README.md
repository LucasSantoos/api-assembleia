# api-assembleia

# Versões
* Java: 17
* Spring-boot: 3.2.0

# Banco de dados
* SQLite

# Como executar
* Instalar Java 17
* Executar o comando na pasta raíz do projeto
```
mvn spring-boot:run -f pom.xml
```
# Associado
* Serviço para cadastrar um novo associado;
* Serviço para atualizar um associado;
* Serviço para buscar um associado;
* Serviço para listar os associados;
* Serviço para deletar um associado.

# Pauta
* Serviço para cadastrar uma nova pauta;
* Serviço para atualizar uma pauta;
* Serviço para buscar uma pauta;
* Serviço para listar as pautas;
* Serviço para deletar uma pauta;
* Serviço para retornar o resultado dos votos de uma pauta, retornando total de votos e porcentagem;
* Serviço para iniciar a sessão com tempo default de 1 minuto de duração;
* Serviço para iniciar a sessão com tempo definido a partir de um parâmetro enviado.

# Voto
* Serviço para votar em uma pauta, informando a opção de voto e o código do associado.

